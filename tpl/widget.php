<?php
$args = array(
	'post_type' => 'testimonial',
	'post_status' => 'publish',
	'tax_query' => array(
    array (
      'taxonomy' => 'testimonial_category',
      'field' => 'slug',
      'terms' => $instance['testimonial_category'],
    )
  )
);
$query = new WP_Query( $args );
if ( $query->have_posts() ) :
	echo '<div class="ballistix-testimonial ' . $instance['widget_type'] . '">';
	while( $query->have_posts() ) : $query->the_post(); ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="post-content">
				<?php the_content(); ?>
				<div class="post-meta">
					<?php if ( @get_post_meta(get_the_id(),'ballistix_testimonial_name', true) ) echo '<span class="ballistix_testimonial_name">' . @get_post_meta(get_the_id(),'ballistix_testimonial_name', true) . '</span>'; ?>
					<?php if ( @get_post_meta(get_the_id(),'ballistix_testimonial_position', true) ) echo '<span class="ballistix_testimonial_position">' . @get_post_meta(get_the_id(),'ballistix_testimonial_position', true) . '</span>'; ?>

					<?php
					if ( @get_post_meta(get_the_id(),'ballistix_testimonial_company', true) ) {
						if ( @get_post_meta(get_the_id(),'ballistix_testimonial_url', true) ) echo '<a class="ballistix_testimonial_url" href="' . @get_post_meta(get_the_id(),'ballistix_testimonial_url', true) . '">';
						echo '<span class="ballistix_testimonial_company">' . @get_post_meta(get_the_id(),'ballistix_testimonial_company', true) . '</span>';
						if ( @get_post_meta(get_the_id(),'ballistix_testimonial_url', true) ) echo '</a>';
					} ?>
				</div>
			</div>
		</article>
	<?php endwhile;
	echo '</div>';
endif;
wp_reset_query(); ?>
