**Plugin Name:** Ballistix: Testimonial  
**Plugin URI:** https://bitbucket.org/ballistixspe/ballistix-testimonial  
**Description:** Adds a custom post for testimonials, includes a widget.  
**Version:** 1.1.0  
**Author:** Marcel Badua  
**Author URI:** https://github.com/marcelbadua  
**License:** GPL2
