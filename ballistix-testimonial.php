<?php
/*
Plugin Name: Ballistix: Testimonial
Plugin URI: https://bitbucket.org/ballistixspe/ballistix-testimonial
Description: Adds a custom post for testimonials, includes a widget.
Version: 1.1.0
Author: Marcel Badua
Author URI: https://github.com/marcelbadua
License: GPL2
*/
/*
Copyright 2019  Marcel Badua  (email : marcel.badua@ballistix.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
if(!class_exists('BALLISTIX_TESTIMONIAL_PLUGIN'))
{
	class BALLISTIX_TESTIMONIAL_PLUGIN
	{
		/**
		 * Construct the plugin object
		 */
		public function __construct()
		{
			// Register custom post types
			require_once(sprintf("%s/inc/construct.php", dirname(__FILE__)));
			$BALLISTIX_TESTIMONIAL_PLUGIN_CONSTRUCT = new BALLISTIX_TESTIMONIAL_PLUGIN_CONSTRUCT();
			//Widget
			require_once(sprintf("%s/inc/widget.php", dirname(__FILE__)));
			$BALLISTIX_TESTIMONIAL_PLUGIN_WIDGET = new BALLISTIX_TESTIMONIAL_PLUGIN_WIDGET();

		} // END public function __construct
		/**
		 * Activate the plugin
		 */
		public static function activate()
		{
			// Do nothing
		} // END public static function activate
		/**
		 * Deactivate the plugin
		 */
		public static function deactivate()
		{
			// Do nothing
		} // END public static function deactivate
	} // END class BALLISTIX_TESTIMONIAL_PLUGIN
} // END if(!class_exists('BALLISTIX_TESTIMONIAL_PLUGIN'))

if(class_exists('BALLISTIX_TESTIMONIAL_PLUGIN'))
{
	// Installation and uninstallation hooks
	register_activation_hook(__FILE__, array('BALLISTIX_TESTIMONIAL_PLUGIN', 'activate'));
	register_deactivation_hook(__FILE__, array('BALLISTIX_TESTIMONIAL_PLUGIN', 'deactivate'));
	// instantiate the plugin class
	$BALLISTIX_TESTIMONIAL_PLUGIN = new BALLISTIX_TESTIMONIAL_PLUGIN();
}
