<?php

/**
 * Register the widget
 */
function BALLISTIX_TESTIMONIAL_PLUGIN_WIDGET_INIT ()
{
    return register_widget('BALLISTIX_TESTIMONIAL_PLUGIN_WIDGET');
}
add_action ('widgets_init', 'BALLISTIX_TESTIMONIAL_PLUGIN_WIDGET_INIT');

/**
 * Class BALLISTIX_TESTIMONIAL_PLUGIN_WIDGET
 */
class BALLISTIX_TESTIMONIAL_PLUGIN_WIDGET extends WP_Widget
{
    /** Basic Widget Settings */
    const WIDGET_NAME        = "Ballistix: Testimonial Widget";
    const WIDGET_DESCRIPTION = "Widget for displaying testimonials";
    public $textdomain;
    public $fields;
    /**
     * Construct the widget
     */
    public function __construct()
    {
        //We're going to use $this->textdomain as both the translation domain and the widget class name and ID
        $this->textdomain = strtolower(get_class($this));

        //Figure out your textdomain for translations via this handy debug print
        //var_dump($this->textdomain);

        //Add fields
        $this->add_field('title', 'Enter title', '', 'text');
        $this->add_field('testimonial_category', 'Testimonial Category', 'default', 'select-category');
        $this->add_field('widget_type', 'Widget Type', 'ballistix-testimonial-slick', 'select-type');

        //Translations
        //load_plugin_textdomain($this->textdomain, false, basename(dirname(__FILE__)) . '/languages' );

        //Init the widget
        parent::__construct($this->textdomain, __(self::WIDGET_NAME, $this->textdomain), array('description' => __(self::WIDGET_DESCRIPTION, $this->textdomain), 'classname' => $this->textdomain));
    }
    /**
     * Widget frontend
     *
     * @param array $args
     * @param array $instance
     */
    public function widget($args, $instance)
    {

        // Enqueue scripts and styles
        $this->plugin_enqueue();

        $title = @apply_filters('widget_title', $instance['title']);
        /* Before and after widget arguments are usually modified by themes */
        echo $args['before_widget'];
        if (!empty($title)) {
            echo $args['before_title'] . $title . $args['after_title'];
        }

        /* Widget output here */
        $this->widget_output($args, $instance);
        /* After widget */
        echo $args['after_widget'];
    }


    /**
     * This function will execute the widget frontend logic.
     * Everything you want in the widget should be output here.
     */
    private function widget_output($args, $instance)
    {
        extract($instance);

        // includes widget frontend template
        include sprintf("%s/../tpl/widget.php", dirname(__FILE__));

    }

    public function plugin_enqueue()
    {

      wp_enqueue_style(
          'slick',
          'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css',
          array(),
          '1.9.0',
          'all'
      );

      wp_enqueue_style(
          'slick-theme',
          'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css',
          array(),
          '1.9.0',
          'all'
      );

      wp_enqueue_script(
          'slick',
          'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js',
          array('jquery'),
          '1.9.0',
          true
      );

      wp_enqueue_script(
          'ballistix-testimonial',
          plugins_url('/js/ballistix-testimonial.js', dirname(__FILE__)),
          array('jquery', 'slick'),
          '1.0.0',
          true
      );

    }
    /**
     * Widget backend
     *
     * @param array $instance
     * @return string|void
     */
    public function form($instance)
    {
        echo '<p>' . self::WIDGET_DESCRIPTION . '</p>';
        /* Generate admin for fields */
        foreach ($this->fields as $field_name => $field_data) {
            if ($field_data['type'] === 'text'):
            ?>
            <p>
              <label for="<?php echo $this->get_field_id($field_name); ?>"><?php _e($field_data['description'], $this->textdomain);?></label>
              <input class="widefat" id="<?php echo $this->get_field_id($field_name); ?>" name="<?php echo $this->get_field_name($field_name); ?>" type="text" value="<?php echo esc_attr(isset($instance[$field_name]) ? $instance[$field_name] : $field_data['default_value']); ?>" /></p>
            <?php
            elseif($field_data['type'] == 'select-type'):
              ?>
                <p><label for="<?php echo $this->get_field_id($field_name); ?>"><?php _e($field_data['description'], $this->textdomain);?></label>
                  <select class="widefat" id="<?php echo $this->get_field_id($field_name); ?>"  name="<?php echo $this->get_field_name($field_name); ?>" >
                    <?php
                      echo "<option value='ballistix-testimonial-slick'" . selected( 'ballistix-testimonial-slick', esc_attr(isset($instance[$field_name]) ? $instance[$field_name] : 'ballistix-testimonial-slick') , FALSE) . ">Carousel</option>";
                      echo "<option value='ballistix-testimonial-loop'" . selected( 'ballistix-testimonial-loop', esc_attr(isset($instance[$field_name]) ? $instance[$field_name] : 'ballistix-testimonial-slick') , FALSE) . ">Loop</option>";
                     ?>
                  </select>
                </p>
              <?php
            elseif($field_data['type'] == 'select-category'):
              $term_query = new WP_Term_Query( array(
               'taxonomy'               => array( 'testimonial_category' ),
               'order'                  => 'ASC',
               'orderby'                => 'title',
               'fields'                 => 'all',
               'hide_empty'             => true,
              ) );
            ?>
              <p><label for="<?php echo $this->get_field_id($field_name); ?>"><?php _e($field_data['description'], $this->textdomain);?></label>
                <select class="widefat" id="<?php echo $this->get_field_id($field_name); ?>"  name="<?php echo $this->get_field_name($field_name); ?>" >
                  <?php
                  foreach ($term_query->terms as $term) :
                    echo "<option value='{$term->slug}'" . selected( $term->slug, esc_attr(isset($instance[$field_name]) ? $instance[$field_name] : 'default') ) . ">{$term->name}</option>";
                  endforeach;
                   ?>
                </select>
              </p>
            <?php
            else:
                echo __('Error - Field type not supported', $this->textdomain) . ': ' . $field_data['type'];
            endif;
        }
    }
    /**
     * Adds a text field to the widget
     *
     * @param $field_name
     * @param string $field_description
     * @param string $field_default_value
     * @param string $field_type
     */
    private function add_field($field_name, $field_description = '', $field_default_value = '', $field_type = 'text')
    {
        if (!is_array($this->fields)) {
            $this->fields = array();
        }
        $this->fields[$field_name] = array(
            'name'          => $field_name,
            'description'   => $field_description,
            'default_value' => $field_default_value,
            'type'          => $field_type,
        );
    }
    /**
     * Updating widget by replacing the old instance with new
     *
     * @param array $new_instance
     * @param array $old_instance
     * @return array
     */
    public function update($new_instance, $old_instance)
    {
        return $new_instance;
    }
}
