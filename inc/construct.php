<?php
/**
 * Class BALLISTIX_TESTIMONIAL_PLUGIN_CONSTRUCT
 */
if (!class_exists('BALLISTIX_TESTIMONIAL_PLUGIN_CONSTRUCT')) {
    class BALLISTIX_TESTIMONIAL_PLUGIN_CONSTRUCT
    {
        public $_post_data	= array(
          'post'         => 'testimonial',
          'menu_icon'    => 'dashicons-thumbs-up',
          'show_in_menu' => TRUE,
          'taxonomy'     => array(
            array('testimonial_category', 'Testimonial Category')
          ),
          'meta' => array(
      			array( 'ballistix_testimonial_name', 'text', 'Name' ),
            array( 'ballistix_testimonial_position', 'text', 'Position' ),
            array( 'ballistix_testimonial_company', 'text', 'Company' ),
            array( 'ballistix_testimonial_url', 'url', 'Company URL' )
      		)
        );
        public function __construct()
        {
          add_action('init', array(&$this, 'init'));
          add_action('admin_init', array(&$this, 'admin_init'));
        }
        public function init()
        {
          if($this->_post_data) {
            $this->create_post_type($this->_post_data['post'], $this->_post_data['menu_icon'], $this->_post_data['show_in_menu']);
            foreach ($this->_post_data['taxonomy'] as $field_name) {
              $this->create_taxonomy($field_name[0], $field_name[1], $this->_post_data['post'] );
            }
          }
          add_action('save_post', array(&$this, 'save_post'));
        }
        /*-------------------------------------------------------------------------------
        Create Custom Post Type
        -------------------------------------------------------------------------------*/
        public function create_post_type($field_name, $menu_icon, $show_in_menu = TRUE)
        {
          register_post_type($field_name, array(
            'labels'       => array(
              'name'                  => _x(sprintf('%ss', ucwords(str_replace("_", " ", $field_name))), 'Post Type General Name', 'text_domain'),
              'singular_name'         => _x(sprintf('%s', ucwords(str_replace("_", " ", $field_name))), 'Post Type Singular Name', 'text_domain'),
              'menu_name'             => __(sprintf('%s', ucwords(str_replace("_", " ", $field_name))), 'text_domain'),
              'name_admin_bar'        => __(sprintf('%s', ucwords(str_replace("_", " ", $field_name))), 'text_domain'),
              'archives'              => __(sprintf('%s Archives', ucwords(str_replace("_", " ", $field_name))), 'text_domain'),
              'parent_item_colon'     => __(sprintf('Parent %s', ucwords(str_replace("_", " ", $field_name))), 'text_domain'),
              'all_items'             => __(sprintf('All %s', ucwords(str_replace("_", " ", $field_name))), 'text_domain'),
              'add_new_item'          => __(sprintf('Add New %s', ucwords(str_replace("_", " ", $field_name))), 'text_domain'),
              'add_new'               => __('Add New', 'text_domain'),
              'new_item'              => __(sprintf('New %s', ucwords(str_replace("_", " ", $field_name))), 'text_domain'),
              'edit_item'             => __(sprintf('Edit %s', ucwords(str_replace("_", " ", $field_name))), 'text_domain'),
              'update_item'           => __(sprintf('Update %s', ucwords(str_replace("_", " ", $field_name))), 'text_domain'),
              'view_item'             => __(sprintf('View %s', ucwords(str_replace("_", " ", $field_name))), 'text_domain'),
              'search_items'          => __(sprintf('Search %s', ucwords(str_replace("_", " ", $field_name))), 'text_domain'),
              'not_found'             => __('Not found', 'text_domain'),
              'not_found_in_trash'    => __('Not found in Trash', 'text_domain'),
              'featured_image'        => __('Featured Image', 'text_domain'),
              'set_featured_image'    => __('Set featured image', 'text_domain'),
              'remove_featured_image' => __('Remove featured image', 'text_domain'),
              'use_featured_image'    => __('Use as featured image', 'text_domain'),
              'insert_into_item'      => __('Insert into item', 'text_domain'),
              'uploaded_to_this_item' => __('Uploaded to this item', 'text_domain'),
              'items_list'            => __('Items list', 'text_domain'),
              'items_list_navigation' => __('Items list navigation', 'text_domain'),
              'filter_items_list'     => __('Filter items list', 'text_domain'),
            ),
            'public'       => true,
            'has_archive'  => true,
            'supports'     => array(
              'title',
              'editor'
            ),
            'menu_icon'    => $menu_icon,
            'hierarchical' => false,
            'show_in_menu' => $show_in_menu
          ));
        }
        /*-------------------------------------------------------------------------------
        Create Custom Taxonomy
        -------------------------------------------------------------------------------*/
        public function create_taxonomy($taxonomy, $name, $post_type)
        {
          $labels = array(
            'name'          => __( sprintf('%s', ucwords(str_replace("_", " ", $name))) ),
            'add_new_item'  => __(sprintf('Add New %s', ucwords(str_replace("_", " ", $name)))),
            'new_item_name' => __(sprintf('New %s', ucwords(str_replace("_", " ", $name)))),
            'singular_name' => _x( sprintf('%s', ucwords(str_replace("_", " ", $name))) , 'taxonomy singular name', 'my_plugin'),
            'search_items' => __(sprintf('Search %s', ucwords(str_replace("_", " ", $name))), 'my_plugin'),
            'popular_items' => __(sprintf('Common %s', ucwords(str_replace("_", " ", $name))), 'my_plugin'),
            'all_items' => __(sprintf('All %s', ucwords(str_replace("_", " ", $name))), 'my_plugin'),
            'edit_item' => __(sprintf('Edit %s', ucwords(str_replace("_", " ", $name))), 'my_plugin'),
            'update_item' => __(sprintf('Edit %s', ucwords(str_replace("_", " ", $name))), 'my_plugin'),
            'add_or_remove_items' => __(sprintf('Remove %s', ucwords(str_replace("_", " ", $name))) , 'my_plugin'),
            'choose_from_most_used' => __(sprintf('Choose from common %s', ucwords(str_replace("_", " ", $name))), 'my_plugin'),
            'not_found' => __(sprintf('No %s found.', ucwords(str_replace("_", " ", $name))), 'my_plugin'),
            'menu_name' => __(sprintf('%s', ucwords(str_replace("_", " ", $name))), 'my_plugin'),
          );
          register_taxonomy($taxonomy, $post_type, array(
            'label'         => __(sprintf('%s', ucwords(str_replace("_", " ", $name)))),
            'labels'        => $labels,
            'show_ui'       => true,
            'show_tagcloud' => false,
            'hierarchical'  => true,
            'public'        => true,
            'rewrite'      => array(
              'slug' => $taxonomy
            ),
          ));
        }
        /**
         * Save the metaboxes for this custom post type
         */
        public function save_post($post_id)
        {
          // verify if this is an auto save routine.
          // If it is our form has not been submitted, so we dont want to do anything
          if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
              return;
          }
          if(isset($_POST['post_type']) && $_POST['post_type'] == $this->_post_data['post'] && current_user_can('edit_post', $post_id)) {
            foreach($this->_post_data['meta'] as $field_name) {
              // Update the post's meta field
              update_post_meta($post_id, $field_name[0], $_POST[$field_name[0]]);
            }
          } else {
            return;
          } // if($_POST['post_type'] == self::POST_TYPE && current_user_can('edit_post', $post_id))
        } // END public function save_post($post_id)
        /*-------------------------------------------------------------------------------
        ADMIN INIT
        -------------------------------------------------------------------------------*/
        public function admin_init()
        {
          // Change ctp title
          add_filter('enter_title_here',array(&$this, 'change_title_text'));
        	// Add metaboxes
      		add_action('add_meta_boxes', array(&$this, 'add_meta_boxes'));
      	} // END public function admin_init()
      	/**
      	 * hook into WP's add_meta_boxes action hook
      	 */
      	public function add_meta_boxes()
      	{
      		// Add this metabox to every selected post
      		add_meta_box(
      			sprintf('ballistix_testimonial_%s_section', $this->_post_data['post']),
      			sprintf('%s Information', ucwords(str_replace("_", " ", $this->_post_data['post']))),
      			array(&$this, 'add_inner_meta_boxes'),
      			$this->_post_data['post']
      	    );
      	} // END public function add_meta_boxes()
  		/**
  		 * called off of the add meta box
  		 */
  		public function add_inner_meta_boxes($post)
  		{
  			// Render the job order metabox
        echo '<table class="meta_table" cellpadding="4">';
        foreach($this->_post_data['meta'] as $field_name){
          echo '<tr valign="top">';
          echo "<th class='metabox_label'><label for='{$field_name[0]}'>{$field_name[2]}</label></th>";
          echo "<td class='metabox_input'><input type='{$field_name[1]}' id='{$field_name[0]}' name='$field_name[0]' value='" . @get_post_meta($post->ID, $field_name[0], true) . "'/></td>";
          echo '</tr>';
        }
        echo '</table>';
  	} // END public function add_inner_meta_boxes($post)
    /*-------------------------------------------------------------------------------
    Edit Add new title
    -------------------------------------------------------------------------------*/
    public function change_title_text($title)
    {
      $screen = get_current_screen();
      if ($this->_post_data['post']) {
        if ($this->_post_data['post'] == $screen->post_type) {
          $title = __(sprintf('Enter %s Here', ucwords(str_replace("_", " ", $this->_post_data['post'] ))), 'text_domain');
        }
      }
      return $title;
    }
  }
}
